﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using System.Text;

namespace NOPITest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is an NPOI demo program.(这是个NPOI演示程序)");

            try
            {
                Console.WriteLine("Start creating .xlsx file.(开始创建.xlsx文件)");
                
                string filePath = $"C:\\temp\\{DateTime.UtcNow.ToString("yyyyMMMddHHmmsss")}.xlsx";

                IWorkbook workbook = CreateWorkbook();
                using (FileStream fileStream = File.Create(filePath))
                {
                    workbook.Write(fileStream);
                    fileStream.Close();
                }

                Console.WriteLine("Run complete(运行完成).");

                Console.WriteLine($"File storage location(文件存放的位置):{filePath}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
            }

            Console.ReadLine();
        }

        public static IWorkbook CreateWorkbook()
        {
            IWorkbook workbook = new XSSFWorkbook();
            Console.WriteLine("Create Sheet1.(创建Sheet1)");
            ISheet sheet1 = workbook.CreateSheet("sheet1");

            Console.WriteLine("Create head font style.(创建头部字体样式)");
            IFont headerFont = workbook.CreateFont();
            headerFont.IsBold = true;
            headerFont.FontHeightInPoints = 13;
            headerFont.FontName = "等线";
            headerFont.Color = IndexedColors.White.Index;

            Console.WriteLine("Create row 1 of Sheet1.(创建Sheet1 的 行1)");
            IRow row1 = sheet1.CreateRow(0);
            Console.WriteLine("Create row 2 of Sheet1.(创建Sheet1 的 行2)");
            IRow row2 = sheet1.CreateRow(1);
            row2.Height = 800;

            Console.WriteLine("Make Cell 1 color style.(制定Cell-1颜色&样式)");
            XSSFColor blueColor = new XSSFColor(new byte[3] { 31, 78, 120 });
            XSSFCellStyle cellStyle1 = HearderCellStyle(workbook, blueColor, headerFont);
            cellStyle1.VerticalAlignment = VerticalAlignment.Center;
            cellStyle1.Alignment = HorizontalAlignment.CenterSelection;
            CellRangeAddress cellRangeAddress1 = new CellRangeAddress(0, 0, 0, 7);
            ICell cell1 = row1.CreateCell(0);
            cell1.CellStyle = cellStyle1;
            cell1.SetCellType(CellType.String);
            cell1.SetCellValue("Cell 1");

            Console.WriteLine("Merge Cells.(合并单元格)");
            SetBorderForMergeCell(sheet1, cellRangeAddress1, cellStyle1);

            string[] row2_cell1_header = new string[] {
                "Cell 1_1","Cell 1_2","Cell 1_3","Cell 1_4","Cell 1_5","Cell 1_6","Cell 1_7","Cell 1_8"
            };

            Console.WriteLine("Set the style for each cell under Cell-1.(为Cell-1 下的每个单元格设置样式)");
            for (int i = 0; i < row2_cell1_header.Length; i++)
            {
                int index = i + 0;
                ICell row2_cell = row2.CreateCell(index);
                row2_cell.CellStyle = cellStyle1;
                row2_cell.SetCellType(CellType.String);
                row2_cell.SetCellValue(row2_cell1_header[i]);
                AutoColumnWidth(sheet1, index);
            }

            Console.WriteLine("Make Cell 2 color style.(制定Cell-2颜色&样式)");
            Console.WriteLine("………………");

            XSSFColor brownColor = new XSSFColor(new byte[3] { 198, 89, 17 });
            XSSFCellStyle cellStyle2 = HearderCellStyle(workbook, brownColor, headerFont);
            CellRangeAddress cellRangeAddress2 = new CellRangeAddress(0, 0, 8, 11);
            ICell cell2 = row1.CreateCell(8);
            cell2.CellStyle = cellStyle2;
            cell2.SetCellType(CellType.String);
            cell2.SetCellValue("Cell 2");
            SetBorderForMergeCell(sheet1, cellRangeAddress2, cellStyle2);

            string[] row2_cell2_header = new string[] {
                "Cell 2_9","Cell 2_10","Cell 2_11","Cell 2_12","Cell 2_13","Cell 2_14","Cell 2_15","Cell 2_16"
            };

            for (int i = 0; i < row2_cell2_header.Length; i++)
            {
                int index = i + 8;
                ICell row2_cell = row2.CreateCell(index);
                row2_cell.CellStyle = cellStyle2;
                row2_cell.SetCellType(CellType.String);
                row2_cell.SetCellValue(row2_cell2_header[i]);
                AutoColumnWidth(sheet1, index);
            }

            Console.WriteLine("Make Cell 3 color style.(制定Cell-3颜色&样式)");
            Console.WriteLine("………………");

            XSSFColor yellowColor = new XSSFColor(new byte[3] { 192, 143, 0 });
            XSSFCellStyle cellStyle3 = HearderCellStyle(workbook, yellowColor, headerFont);
            CellRangeAddress cellRangeAddress3 = new CellRangeAddress(0, 0, 12, 19);
            ICell cell3 = row1.CreateCell(12);
            cell3.CellStyle = cellStyle3;
            cell3.SetCellType(CellType.String);
            cell3.SetCellValue("Cell 3");
            SetBorderForMergeCell(sheet1, cellRangeAddress3, cellStyle3);

            string[] row2_cell3_header = new string[] {
                "Cell 3_17","Cell 3_18","Cell 3_19","Cell 3_20","Cell 3_21","Cell 3_22","Cell 3_23","Cell 3_24"
            };

            for (int i = 0; i < row2_cell3_header.Length; i++)
            {
                int index = i + 12;
                ICell row2_cell = row2.CreateCell(index);
                row2_cell.CellStyle = cellStyle3;
                row2_cell.SetCellType(CellType.String);
                row2_cell.SetCellValue(row2_cell3_header[i]);
                AutoColumnWidth(sheet1, index);
            }

            return workbook;
        }

        /// <summary>
        /// 自动调整列的宽度
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="index"></param>
        private static void AutoColumnWidth(ISheet sheet, int index)
        {
            int columnWidth = sheet.GetColumnWidth(index) / 256;
            for (int rowIndex = 1; rowIndex <= sheet.LastRowNum; rowIndex++)
            {
                IRow row = sheet.GetRow(rowIndex);
                ICell cell = row.GetCell(index);
                int contextLength = Encoding.UTF8.GetBytes(cell.ToString()).Length;
                columnWidth = columnWidth < contextLength ? contextLength : columnWidth;

            }
            sheet.SetColumnWidth(index, columnWidth * 300);
        }

        /// <summary>
        /// 头部单元格样式
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="xssfColor"></param>
        /// <param name="headerFont"></param>
        /// <returns></returns>
        private static XSSFCellStyle HearderCellStyle(IWorkbook workbook, XSSFColor xssfColor, IFont headerFont)
        {
            XSSFCellStyle cellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            cellStyle.FillForegroundColorColor = xssfColor;
            cellStyle.FillBackgroundColor = xssfColor.Indexed;
            cellStyle.FillPattern = FillPattern.SolidForeground;
            cellStyle.Alignment = HorizontalAlignment.CenterSelection;
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
            cellStyle.SetFont(headerFont);
            return cellStyle;
        }

        /// <summary>
        /// 合并单元格 AND 设定边框
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="cellRangeAddress"></param>
        /// <param name="cellStyle"></param>
        public static void SetBorderForMergeCell(ISheet sheet, CellRangeAddress cellRangeAddress, XSSFCellStyle cellStyle)
        {
            sheet.AddMergedRegion(cellRangeAddress);

            int firstRow = cellRangeAddress.FirstRow;
            int lastRow = cellRangeAddress.LastRow;
            int firstCol = cellRangeAddress.FirstColumn;
            int lastCol = cellRangeAddress.LastColumn;

            cellStyle.VerticalAlignment = VerticalAlignment.Center;
            cellStyle.Alignment = HorizontalAlignment.CenterSelection;

            for (int i = firstRow; i <= lastRow; i++)
            {
                IRow row = sheet.GetRow(i);

                if (row == null)
                {
                    row = sheet.CreateRow(i);
                }

                for (int j = firstCol; j <= lastCol; j++)
                {
                    ICell cell1 = row.GetCell(j);

                    if (cell1 == null)
                    {
                        cell1 = row.CreateCell(j);
                    }

                    if (j == firstCol)
                    {
                        cellStyle.BorderTop = BorderStyle.Thin;
                        cellStyle.BorderBottom = BorderStyle.Thin;
                        cellStyle.BorderLeft = BorderStyle.Thin;
                    }
                    else if (j == lastCol)
                    {
                        cellStyle.BorderTop = BorderStyle.Thin;
                        cellStyle.BorderBottom = BorderStyle.Thin;
                        cellStyle.BorderRight = BorderStyle.Thin;
                    }
                    else
                    {
                        cellStyle.BorderTop = BorderStyle.Thin;
                        cellStyle.BorderBottom = BorderStyle.Thin;
                    }

                    cell1.CellStyle = cellStyle;
                }
            }
        }
    }
}
